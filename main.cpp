#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include <string>

//Screen Attributes
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

//The surfaces
SDL_Surface *board = NULL;
SDL_Surface *screen = NULL;

//Event Structures
SDL_Event event;

//The clip regions of the sprite sheet
SDL_Rect clips[ 4 ];


SDL_Surface *load_image( std::string filename )
{
    //Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename.c_str() );

    //If nothing went wrong in loading the image
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }

    //Return the optimized image
    return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

bool init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return false;
    }

    //Set the window caption
    SDL_WM_SetCaption( "Drifts", NULL );

    //If everything OK
    return true;
}

bool load_files()
{
    //Load Images
    board = load_image( "img/fundo.jpg" );

    //Load the button sprite sheet
//   sprites = load_image( "img_b.jpg" );

    //If there was an error loading the images
    if( board == NULL )
    {
        return false;
    }

    //If everything loaded fine
    return true;
}

void clean_up()
{
    //Free the loaded image
    SDL_FreeSurface(board);

    //Quit SDL
    SDL_Quit();
}

// The cursor
// -----------------------------------------------------------
class Cursor
{
    private:
        SDL_Surface *cursor;
        SDL_Rect dest;
        int x_pos, y_pos;
    public:
        Cursor();
        Cursor(std::string path, int x, int y);
        ~Cursor();
        void reload();
};

Cursor::Cursor(){
    cursor = load_image("img/cursor3.jpg");
    x_pos = 0;
    y_pos = 0;
}
Cursor::Cursor(std::string path, int x, int y){
    cursor = load_image(path);
    x_pos = x;
    y_pos = y;
}
Cursor::~Cursor(){
}
void Cursor::reload(){
    SDL_GetMouseState(&x_pos, &y_pos);
    dest.x = x_pos;
    dest.y = y_pos;
    SDL_BlitSurface(cursor, NULL, screen, &dest);
}

//------------------------------------------------------------

// The Objects
// -----------------------------------------------------------
class Balls
{
    private:
        SDL_Surface *ball;
        int velocity, x_bth_pos, y_bth_pos;
        SDL_Rect destino;
    public:
        Balls();
        ~Balls();
        void draw();
        void falls();
};

Balls::Balls(){
    ball = load_image("img/balls.jpg");
    x_bth_pos = 100;
    y_bth_pos = 100;
}
Balls::~Balls(){
}

void Balls::falls(){
    destino.x +=  1;
    destino.y += 1;
    SDL_BlitSurface(ball, NULL, screen, &destino);
}


// -----------------------------------------------------------

int main( int argc, char* args[] )
{
    //Quit flag
    bool quit = false;

    //Initialize
    if( init() == false )
    {
        return 1;
    }

    //Load the files
    if( load_files() == false )
    {
        return 1;
    }

    //Clip the sprite sheet
   // set_clips();

    // "Disfigures" mouse cursor
    SDL_ShowCursor(0);


    //Creates a object instance
    Cursor mouse;

    //Creates a object instance
    Balls ball;

    //While the user hasn't quit
    while( quit == false )
    {

        SDL_FillRect(screen,NULL,0); // "Limpa a tela"
        apply_surface( 0, 0, board, screen, NULL ); // Recarrega o pano de fundo

        //If there's events to handle
       if( SDL_PollEvent( &event ) )
        {
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
            }
        }
         ball.falls();
        mouse.reload();

        SDL_UpdateRect(screen, 0, 0, 0, 0);

        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;
        }
    }

    //Clean up
    clean_up();

    return 0;
}
